import axios from 'axios';
const endpoint = "http://localhost:8000";

export function getTodosAll() {
  return axios({
    method: 'get',
    headers: { 
      'Content-Type': 'application/json'
    },
    url: endpoint + "/todos"
  });
}

export function getTodosIncompleted() {
  return axios.get(endpoint + "/todos/incompleted");
}

export function getTodosCompleted() {
  return axios.get(endpoint + "/todos/completed");
}

export function addTodo(text) {
  return axios({
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    url: endpoint + "/todos",
    data: { "text": text }
  });
}

export function updateTodo(id, text) {
  return axios({
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    url: endpoint + "/todos/" + id,
    data: { "text": text }
  });
}

export function removeTodo(id) {
  return axios({
    method: 'delete',
    headers: { 'Content-Type': 'application/json' },
    url: endpoint + "/todos/" + id
  });
}

export function setCompleted(id, completed) {
  const action = completed ? "complete" : "incomplete";
  return axios({
    method: 'post',
    headers: { 'Content-Type': 'application/json' },
    url: endpoint + "/todos/" + id + "/" + action
  });
}
