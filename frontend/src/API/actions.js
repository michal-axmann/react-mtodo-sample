export const FETCH_TODOS = "FETCH_TODOS";
export const FETCH_TODOS_DONE = "FETCH_TODOS_DONE";
export const FETCH_TODO_DONE = "FETCH_TODO_DONE";
export const FILTER_SET = "FILTER_SET";
export const SET_COMPLETED = "SET_COMPLETED";
export const ADD_TODO = "ADD_TODO";
export const INSERT_TODO_DONE = "INSERT_TODO_DONE";
export const UPDATE_TODO = "UPDATE_TODO";
export const UPDATE_TODO_DONE = "UPDATE_TODO_DONE";
export const REMOVE_TODO = "REMOVE_TODO";
export const REMOVE_TODO_DONE = "REMOVE_TODO_DONE";
export const API_ERROR = "API_ERROR";
export const API_CLEAR_ERRORS = "API_CLEAR_ERRORS";

export const SA_ADD_NOTIFICATION    = "SA_ADD_NOTIFICATION";
export const RA_REMOVE_NOTIFICATION = "RA_REMOVE_NOTIFICATION";
export const RA_PUSH_NOTIFICATION   = "RA_PUSH_NOTIFICATION";

export function fetchTodos() {
  return {
    type: FETCH_TODOS
  };
}

export function fetchTodosDone(result = {}) {
  return {
    result: result,
    type: FETCH_TODOS_DONE
  };
}

export function fetchTodoDone(result = {}) {
  return {
    result: result,
    type: FETCH_TODO_DONE
  };
}

export function insertTodoDone(result = {}) {
  return {
    result: result,
    type: INSERT_TODO_DONE
  };
}

export function setFilter(filter = null) {
  return {
    filter: filter,
    type: FILTER_SET
  };
}

export function removeTodoDone(id) {
  return {
    id: id,
    type: REMOVE_TODO_DONE
  };
}

export function setCompleted(id, completed) {
  return {
    id: id,
    completed: completed,
    type: SET_COMPLETED
  };
}

export function removeTodo(id) {
  return {
    id: id,
    type: REMOVE_TODO
  };
}

export function addTodo(text) {
  return {
    text: text,
    type: ADD_TODO
  };
}

export function updateTodo(id, text) {
  return {
    id: id,
    text: text,
    type: UPDATE_TODO
  };
}

export function apiAddError(text) {
  return {
    text: text,
    type: API_ERROR
  };
}

export function apiClearErrors() {
  return {
    type: API_CLEAR_ERRORS
  };
}

export function saAddNotification(text, timeout = 3000, style = { color: "grey", backgroundColor: "#eee" }) {
  return {
    text: text,
    style: style,
    timeout: timeout,
    type: SA_ADD_NOTIFICATION
  };
}

export function raPushNotification(notification) {
  return {
    notification: notification,
    type: RA_PUSH_NOTIFICATION
  };
}

export function raRemoveNotification(id) {
  return {
    id: id,
    type: RA_REMOVE_NOTIFICATION
  };
}
