export default function(state = {}, action) {
  switch (action.type) {
  case 'FETCH_TODOS_DONE':
    return {
      ...state,
      todos: action.result,
      app: {
        ...state.app,
        loading: false
      }
    };
  case 'INSERT_TODO_DONE':
    return {
      ...state,
      todos:[...state.todos, action.result]
    };
  case 'REMOVE_TODO_DONE':
    return {
      ...state,
      todos: state.todos.filter(
        (todo) => todo.id !== action.id
      )
    };
  case 'FETCH_TODO_DONE':
    return {
      ...state,
      todos: state.todos.map(
        (todo) => todo.id === action.result.id ? action.result : todo
      )
    };
  case 'API_ERROR':
    return {
      ...state,
      error: action.text
    };
  case 'API_CLEAR_ERRORS':
    return {
      ...state,
      error: null
    };
  case 'FILTER_SET':
    return {
      ...state,
      filter: action.filter
    };
  case 'RA_PUSH_NOTIFICATION':
    return {
      ...state,
      app: {
        ...state.app,
        notifications: [
          action.notification,
          ...state.app.notifications
        ]
      }
    };
  case 'RA_REMOVE_NOTIFICATION':
    let newNotifications = state.app.notifications.filter((e) => {
      return e.id !== action.id;
    });

    return {
      ...state,
      app: {
        ...state.app,
        notifications: newNotifications
      }
    };    
  default:
    return state;
  }
}
