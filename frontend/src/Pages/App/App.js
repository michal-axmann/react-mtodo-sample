import PropTypes from 'prop-types';
import React, { Component } from 'react';
import TodoList           from 'Organisms/TodoList/TodoList';
import Filter             from 'Organisms/Filter/Filter';

import { connect } from "react-redux";

class App extends Component {
  render() {
    const { notifications } = this.props;
    const notificationsLog = notifications.map(n => <div key={n.id} className='error'>{n.text}</div>);  /* TODO move to a separate component */

    return (
      <div className='pageTodoList'>
        <h1>mTODO | Simple TodoList App</h1>
        <Filter/>
        <TodoList />
        {notificationsLog}
      </div>
    );
  }
}

App.propTypes = {
  notifications: PropTypes.array
};

const mapStateToProps = state => {
  return {
    notifications: state.app.notifications
  };
};

const mapDispatchToProps = dispatch => {
  return {
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
