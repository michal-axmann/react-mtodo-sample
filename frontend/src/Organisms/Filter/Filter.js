import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from "react-redux";
import { SegmentedControl } from 'segmented-control';
import * as API from 'API/actions';
import { createSelector } from 'reselect';

class Filter extends Component {
  changeFilter(filterValue) {
    const { setFilter } = this.props;
    setFilter(filterValue);
    this.deleteAll = this.deleteAll.bind(this);
    this.completeAll = this.completeAll.bind(this);
    this.reloadAll = this.reloadAll.bind(this);
  }

  reloadAll() {
    const { fetchTodos } = this.props;
    fetchTodos();
  }

  completeAll() {
    const { allIncompleted, setCompleted } = this.props;
    allIncompleted.forEach(todo => setCompleted(todo.id, true));
  }

  deleteAll() {
    const { allCompleted, removeTodo } = this.props;
    allCompleted.forEach(todo => removeTodo(todo.id));
  }

  render() {
    const { filter, allCompleted, allIncompleted, allTodos } = this.props;

    const buttonReload = allTodos.length === 0 ? <input type='button' className='button blue small' value='Reload' onClick={this.reloadAll} /> : "";
    const buttonMarkAsCompleted = filter !== true && allTodos.length > 0 ? <input type='button' className='button blue small' value='Mark all as "completed"' onClick={this.completeAll} /> : "";
    const buttonDeleteCompleted = filter !== false && allTodos.length > 0 ? <input type='button' className='button blue small' value='Delete completed' onClick={this.deleteAll} /> : "";

    return (<div><SegmentedControl
      name='oneDisabled'
      options={[
        { label: `All (${allTodos.length})`, value: null, default: true },
        { label: `Completed (${allCompleted.length})`, value: true },
        { label: `Incompleted (${allIncompleted.length})`, value: false }
      ]}
      setValue={filterValue => this.changeFilter(filterValue)}
      style={{ width: 800, color: '#269CE9' }}
    />
    <br/>
    { buttonReload }
    { buttonDeleteCompleted }
    { buttonMarkAsCompleted }
    <br/><br/>
    </div>);
  }
}

Filter.propTypes = {
  filter: PropTypes.bool,
  allCompleted: PropTypes.array,
  allIncompleted: PropTypes.array,
  allTodos: PropTypes.array
};

const getAllCompleted = createSelector(
  state => state.todos,
  (todos) => {
    return todos.filter(t => t.completed);
  }
);

const getAllIncompleted = createSelector(
  state => state.todos,
  (todos) => {
    return todos.filter(t => !t.completed);
  }
);

const mapStateToProps = state => {
  return {
    filter: state.filter,
    allCompleted: getAllCompleted(state),
    allIncompleted: getAllIncompleted(state),
    allTodos: state.todos
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setFilter : (filter) => dispatch(API.setFilter(filter)),
    setCompleted : (id, completed) => dispatch(API.setCompleted(id, completed)),
    removeTodo : (id) => dispatch(API.removeTodo(id)),
    fetchTodos : () => dispatch(API.fetchTodos())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Filter);
