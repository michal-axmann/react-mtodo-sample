export const TODO_ADD       = "TODO_ADD";
export const TODO_UPDATE    = "TODO_UPDATE";
export const TODO_GET_ALL   = "TODO_GET_ALL";

export function addTodo(text) {
  return {
    text: text,
    type: TODO_ADD
  };
}

export function getAllTodos() {
  return {
    type: TODO_GET_ALL
  };
}
