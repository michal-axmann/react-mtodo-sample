import PropTypes from 'prop-types';
import React, { Component } from 'react';
import { connect } from "react-redux";
import * as API from 'API/actions';
import TodoEdit from 'Atoms/TodoEdit';

class TodoItem extends Component {
  constructor(props) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.clickRemoveTodo = this.clickRemoveTodo.bind(this);
    this.state = {
      editable: false
    };
  }

  clickRemoveTodo(event) {
    const { todo, removeTodo } = this.props;
    removeTodo(todo.id);
  }

  handleInputChange(event) {
    const { todo, setCompleted } = this.props;
    const checked = event.target.checked;
    setCompleted(todo.id, checked);
  }

  render() {
    const { todo } = this.props;

    let displayControl = todo.completed ? <div className='todoText completed'>{todo.text}</div> : <TodoEdit id={todo.id} text={todo.text} completed={todo.completed} />;

    return (<li className='todo'>
      <input type='checkbox'
        className='cbComplete'
        checked={todo.completed}
        onChange={this.handleInputChange} />
      {displayControl}
      <input type='button' className='button small red destroy floatRight' value='Remove' onClick={this.clickRemoveTodo}/>
    </li>);
  }
}

TodoItem.propTypes = {
  todo: PropTypes.object
};

const mapStateToProps = state => {
  return {
  };
};

const mapDispatchToProps = dispatch => {
  return {
    setCompleted : (id, completed) => dispatch(API.setCompleted(id, completed)),
    removeTodo : (id) => dispatch(API.removeTodo(id))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoItem);

// <span className={textCSS}>{todo.text}</span>
