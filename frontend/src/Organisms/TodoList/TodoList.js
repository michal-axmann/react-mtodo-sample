import PropTypes            from 'prop-types';
import React, { Component } from 'react';
import { connect }          from "react-redux";
import * as API             from 'API/actions';
import TodoItem             from 'Organisms/TodoList/TodoItem';
import Loading              from 'Atoms/Loading';
import TodoNew              from 'Atoms/TodoNew';

class TodoList extends Component {
  componentDidMount() {
    const { fetchTodos } = this.props;
    fetchTodos();
  }

  render() {
    const { todos, filter, loading } = this.props;
    
    if (loading) {
      return <div><Loading/></div>;
    };

    let selectedTodos = filter !== null ? todos.filter((e) => e.completed === filter) : todos;

    let ts = selectedTodos.map((todo) => {
      return <TodoItem key={todo.id} todo={todo} />;
    });

    let todoNew = <TodoNew/>;

    return (<ul className='todolist'>
      {ts}
      {todoNew}
    </ul>);
  }
}

TodoList.propTypes = {
  todos: PropTypes.array,
  filter: PropTypes.bool
};

const mapStateToProps = state => {
  return {
    loading: state.app.loading,
    filter: state.filter,
    todos: state.todos
  };
};

const mapDispatchToProps = dispatch => {
  return {
    fetchTodos : () => dispatch(API.fetchTodos())
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoList);
