import 'babel-polyfill'; // generator support
import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import App from 'Pages/App/App';
import initStore from 'store/storeFactory';

require("../styles/application.scss");

const store = initStore();

function render() {
  ReactDOM.render(
    <Provider store={ store }>
      <App name='react-sample'/>
    </Provider>
    , document.getElementById('react-root')
  );
}

render();
