import PropTypes            from 'prop-types';
import React, { Component } from 'react';
import { connect } from "react-redux";
import * as API from 'API/actions';

class TodoEdit extends Component {
  constructor(props) {
    super(props);
    this.state = {
      defaultValue: this.props.text, // defaultValue when user enters the input
      typed: this.props.text
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleKeyDown = this.handleKeyDown.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.backToDefault = this.backToDefault.bind(this);
  }

  handleInputChange(event) {
    this.setState({ ...this.state, typed: event.target.value });
  }

  handleKeyDown(event) {
    const { updateTodo } = this.props;

    switch (event.keyCode) {
    case 13: // ENTER
      const typedClean = this.state.typed.trim();
      this.setState({ ...this.state, defaultValue: typedClean, typed: typedClean });
      updateTodo(this.props.id, typedClean);
      break;
    case 27:  // ESC
      this.backToDefault();
      break;
    }
  }

  onBlur(event) {
    this.backToDefault();
  }

  backToDefault() {
    this.setState({ ...this.state, typed: this.state.defaultValue });
  }
  
  render() {
    return (<input className='todoEdit' type='text'
      value={this.state.typed}
      onChange={this.handleInputChange}
      onKeyDown={this.handleKeyDown}
      onBlur={this.onBlur}
      placeholder='Enter some task'
    />
    );
  }
}

TodoEdit.propTypes = {
  text: PropTypes.string
};

const mapStateToProps = state => {
  return {
  };
};

const mapDispatchToProps = dispatch => {
  return {
    updateTodo : (id, text) => dispatch(API.updateTodo(id, text))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoEdit);
