import React, { Component } from 'react';
import { connect } from "react-redux";

class Loading extends Component {
  changeFilter(filterValue) {
    const { setFilter } = this.props;
    setFilter(filterValue);
  }

  render() {
    return <div>Loading...</div>;
  }
}

const mapStateToProps = state => {
  return {
  };
};

const mapDispatchToProps = dispatch => {
  return {
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Loading);
