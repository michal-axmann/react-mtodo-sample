import React, { Component } from 'react';
import { connect } from "react-redux";
import * as API from 'API/actions';

class TodoNew extends Component {
  constructor(props) {
    super(props);
    this.state = {
      typed: ""
    };
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleKeyPress = this.handleKeyPress.bind(this);
  }

  handleInputChange(event) {
    this.setState({ typed: event.target.value });
  }

  handleKeyPress(event) {
    const { addTodo } = this.props;
    if (event.key === 'Enter') {
      addTodo(this.state.typed.trim());
      this.setState({ typed: "" });
    }
  }

  render() {
    return (<li><input className='todoNew' type='text'
      value={this.state.typed}
      onChange={this.handleInputChange}
      onKeyPress={this.handleKeyPress}
      placeholder='Add new task here...'
    />
    </li>
    );
  }
}

const mapStateToProps = state => {
  return {
  };
};

const mapDispatchToProps = dispatch => {
  return {
    addTodo : (text) => dispatch(API.addTodo(text))
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(TodoNew);
