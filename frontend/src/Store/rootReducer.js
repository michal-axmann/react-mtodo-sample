import initialState           from 'store/initState';
import apiReducer             from 'API/reducer';

// TODO somewhat, the comibine reducer does not work. Check later and remove workaround.

export default function rootReducer(state = initialState, action) {
  let newState = Object.assign({}, state);
  newState = apiReducer(newState, action);
  return newState;
}
