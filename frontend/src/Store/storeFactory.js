import { applyMiddleware, createStore, compose } from 'redux';
import { rootSaga } from './rootSaga';
import createSagaMiddleware from 'redux-saga';
import rootReducer from 'store/rootReducer';

export default function initStore() {
  const sagaMiddleware = createSagaMiddleware();

  const reduxDevTools =
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__();

  const store = createStore(
    rootReducer,
    compose(applyMiddleware(sagaMiddleware), reduxDevTools)
  );

  sagaMiddleware.run(rootSaga);

  return store;
}