import { takeEvery, put, call } from 'redux-saga/effects';

import * as API from 'API';
import { fetchTodosDone, fetchTodoDone, insertTodoDone, removeTodoDone, apiAddError, saAddNotification, raPushNotification, raRemoveNotification } from 'API/actions';

export function* rootSaga() {
  yield takeEvery('FETCH_TODOS', sagaFetchTodos);
  yield takeEvery('ADD_TODO', sagaAddTodo);
  yield takeEvery('UPDATE_TODO', sagaUpdateTodo);
  yield takeEvery('REMOVE_TODO', sagaRemoveTodo);
  yield takeEvery('SET_COMPLETED', sagaSetCompleted);
  yield takeEvery('SA_ADD_NOTIFICATION', sagaAddNotification);

}

function delay(ms) {
  return new Promise(resolve => setTimeout(() => resolve(true), ms));
}

function* sagaFetchTodos() {
  try {
    let response = yield call(API.getTodosAll);
    let result = response.data;
    yield put(fetchTodosDone(result));
  } catch (error) {
    yield put(saAddNotification("Error: Can't read todos from server. Check if server is running", 3000));
  }  
}

function* sagaAddTodo(action) {
  try {
    let response = yield call(API.addTodo, action.text);
    let result = response.data;
    yield put(insertTodoDone(result));
  } catch (error) {
    yield put(saAddNotification("Error: Can't add todo item. Check if server is running", 3000));
  }
}

function* sagaRemoveTodo(action) {
  try {
    let response = yield call(API.removeTodo, action.id);
    // BE: remove should return some value
    yield put(removeTodoDone(action.id));
  } catch (error) {
    yield put(saAddNotification("Error: Can't remove todo item. Check if server is running", 3000));
  }
}

function* sagaUpdateTodo(action) {
  try {
    let response = yield call(API.updateTodo, action.id, action.text);
    let result = response.data;
    yield put(fetchTodoDone(result));
  } catch (error) {
    yield put(saAddNotification("Error: Can't update todo item. Check if server is running", 3000));
  }
}

function* sagaSetCompleted(action) {
  try {
    let response = yield call(API.setCompleted, action.id, action.completed);
    let result = response.data;
    yield put(fetchTodoDone(result));
  } catch (error) {
    yield put(saAddNotification("Error: Can't set item as completed. Check if server is running", 3000));
  }
}

function getGuid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }
  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

function* sagaAddNotification(action) {
  const id = getGuid();

  let notification = {
    id: id,
    text: action.text
  };

  yield put(raPushNotification(notification));
  yield delay(action.timeout);
  yield put(raRemoveNotification(id));
}
