var path = require('path');
var HardSourceWebpackPlugin = require('hard-source-webpack-plugin');

module.exports = {
  devtool: 'eval',
  mode: 'development',
  entry: {
    app: [
      'babel-polyfill',
      'webpack-dev-server/client?http://localhost:8000',
      './src/index.jsx'
    ]
  },
  output: {
    path: path.join(__dirname, 'build', 'static'),
    publicPath: '/static/',
    filename: 'bundle.js'
  },
  resolve: {
    extensions: ['.js', '.jsx', '.es6', 'json'],
    modules: [
      path.join(__dirname, '/src'),
      path.join(__dirname, '/img'),
      path.join(__dirname, '/node_modules')
    ]
  },

  module: {
    rules: [{
      test: /\.jsx?$/,
      loader: "babel-loader",
      query: {
        presets: [
          ["env",
            {
              "targets": {
                "browsers": ["last 2 versions"]
              }
            }
          ],
          "react"
        ]
      },
      include: path.join(__dirname, 'src')
    },
    {
      test: /\.scss$/,
      loaders: ["style-loader", "css-loader", "sass-loader"]
    },
    {
      test: /\.css$/,
      loaders: ["style-loader", "css-loader"]
    },
    {
      test: /\.(png|gif|jpeg|jpg|ttf|eot|svg|woff|woff2|otf|ico)$/,
      loaders: ["file-loader"]
    }
    ]
  },
  plugins: [
    new HardSourceWebpackPlugin()
  ],
  devServer:{
    historyApiFallback: {
      rewrites:[
        {
          from: /^\/?(?!static).*/,
          to: '/index.html'
        }
      ],
      index: '/'
    },
    headers: {
      "Access-Control-Allow-Origin": "*",
      "Access-Control-Allow-Methods": "GET, POST, PUT, DELETE, PATCH, OPTIONS",
      "Access-Control-Allow-Headers": "X-Requested-With, content-type, Authorization"
    },
    port: 8000,
    proxy: {
      '/todos': 'http://localhost:8080'
    }
  }
};
