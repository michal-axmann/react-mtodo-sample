# react-mtodo-sample

Sample app based on React/Redux. It allows users to manage task list items.

## Frontend

### Development 

yarn start

Runs at http://localhost:8000/. 

Adding items - Simple start typing new item at the very bottom of the task list.

Removing items - Items can be removed one by one by clicking the "Remove button" or all completed items at once by clicking the "Delete all completed"

Filtering - Filtering is done through control at the top. 

Completing the tasks - You can either mark each item as completed individually by clicking the checkbox or mark all incomplete by clicking the button "Mark all as completed".

For your convenience, the number of items in each category is displayed.

Error handling - Should there be an issue with backend, appropriate error messages are added into the notification area.

### Production

yarn build